use crossbeam::channel::{Receiver, Sender};
use rlua::{Function, Lua};
use std::collections::HashMap;
use std::fs::{read_dir, read_to_string};
use std::path::PathBuf;
use std::time::Duration;

pub enum FromEventType {
    RegisterAboutWindow(String, bool, bool, bool),
    RegisterLabel(String, String),
    RegisterButton(String, String),
}

pub struct FromPluginEvent {
    pub plugin: u16,
    pub event_type: FromEventType,
}

pub enum ToEventType {
    ButtonClick(String),
}

pub struct ToPluginEvent {
    pub plugin: Option<u16>,
    pub event_type: ToEventType,
}

struct PluginState {
    id: u16,
    name: String,
    lua: Lua,
}

pub struct PluginManager {
    tx: Sender<FromPluginEvent>,
    rx: Receiver<ToPluginEvent>,
    plugins: HashMap<u16, PluginState>,
}

impl PluginManager {
    pub fn new(tx: Sender<FromPluginEvent>, rx: Receiver<ToPluginEvent>) -> Self {
        let mut plugins = HashMap::new();
        let dir = read_dir("assets/plugins");
        if let Ok(dir) = dir {
            for (id, e) in dir.enumerate() {
                if let Ok(e) = e {
                    // TODO: Slap all files in a dir together?
                    let path = e.path();
                    if path.is_file() {
                        load_lua(path, tx.clone(), &mut plugins, id as u16);
                    }
                }
            }
        }
        Self { tx, rx, plugins }
    }

    pub fn plugins(&self) -> Vec<(u16, String)> {
        self.plugins
            .iter()
            .map(|(id, s)| (*id, s.name.to_owned()))
            .collect()
    }

    pub fn run(&mut self) {
        loop {
            match self.rx.try_recv() {
                Ok(e) => {
                    match e.event_type {
                        ToEventType::ButtonClick(sid) => {
                            if let Some(id) = e.plugin {
                                if let Some(plugin) = self.plugins.get(&id) {
                                    plugin.lua.context(|c| {
                                        let globals = c.globals();
                                        let callback =
                                            globals.get::<_, Function>("button_click").unwrap();

                                        // Don't care if call fails
                                        callback.call::<_, ()>(sid).ok();
                                    });
                                }
                            } else {
                                // TODO: All
                            }
                        }
                    }
                }
                Err(crossbeam::channel::TryRecvError::Empty) => {
                    std::thread::sleep(Duration::from_millis(1));
                }
                Err(_) => {
                    panic!("ToPlugin channel closed");
                }
            }
        }
    }
}

fn load_lua(
    path: PathBuf,
    tx: Sender<FromPluginEvent>,
    plugins: &mut HashMap<u16, PluginState>,
    id: u16,
) {
    let fullname = path
        .file_name()
        .unwrap()
        .to_str()
        .unwrap()
        .split('.')
        .collect::<Vec<_>>();
    let name = fullname.first().unwrap().to_string();

    if let Ok(contents) = read_to_string(path) {
        let lua = Lua::new();
        lua.context(|c| {
            let globals = c.globals();

            let txfn = tx.clone();
            let reg_win = c.create_function(move |_ctx, (title, resizable, visible, closable): (String, bool, bool, bool)| {
                txfn.send(FromPluginEvent {
                    plugin: id,
                    event_type: FromEventType::RegisterAboutWindow(title, resizable, visible, closable),
                }).unwrap();
                Ok(())
            }).unwrap();
            globals.set("register_about_window", reg_win).unwrap();

            let txfn = tx.clone();
            let reg_lbl = c.create_function(move |_ctx, (sid, text): (String, String)| {
                txfn.send(FromPluginEvent {
                    plugin: id,
                    event_type: FromEventType::RegisterLabel(sid, text),
                }).unwrap();
                Ok(())
            }).unwrap();
            globals.set("register_about_label", reg_lbl).unwrap();

            let txfn = tx.clone();
            let reg_btn = c.create_function(move |_ctx, (sid, text): (String, String)| {
                txfn.send(FromPluginEvent {
                    plugin: id,
                    event_type: FromEventType::RegisterButton(sid, text),
                }).unwrap();
                Ok(())
            }).unwrap();
            globals.set("register_about_button", reg_btn).unwrap();

            // TODO: Proper error handling
            c.load(&contents).set_name(&name).unwrap().exec().expect("Failed to exec plugin");
        });

        plugins.insert(id, PluginState { id, name, lua });
    }
}
