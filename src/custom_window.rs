use crate::plugin_mgr::{ToEventType, ToPluginEvent};
use bevy_egui::egui::{CtxRef, Frame, Response, Window};
use crossbeam::channel::Sender;

/// Widgets aren't clone friendly
#[derive(Clone)]
pub enum MiniWidget {
    Label(String),
    Button(String),
}

pub struct CustomWindow {
    plugin_id: u16,
    pub title: String,
    resizable: bool,
    visible: bool,
    closable: bool,
    widgets: Vec<(String, MiniWidget)>,
}

impl CustomWindow {
    pub fn new(id: u16, title: String) -> Self {
        Self {
            plugin_id: id,
            title,
            resizable: false,
            visible: false,
            closable: true,
            widgets: vec![],
        }
    }

    pub fn show(
        &mut self,
        ctx: &CtxRef,
        frame: Frame,
        tx: &Sender<ToPluginEvent>,
    ) -> Option<Response> {
        if !self.visible {
            return None;
        }

        let id = self.plugin_id;

        let mut win = Window::new(&self.title)
            .resizable(self.resizable)
            .frame(frame);

        if self.closable {
            win = win.open(&mut self.visible);
        }

        let widgets = self.widgets.clone();
        win.show(ctx, |ui| {
            if widgets.len() > 0 {
                for (sid, widget) in widgets {
                    match widget {
                        MiniWidget::Label(text) => {
                            ui.label(text);
                        }
                        MiniWidget::Button(text) => {
                            if ui.button(text).clicked {
                                tx.send(ToPluginEvent {
                                    plugin: Some(id),
                                    event_type: ToEventType::ButtonClick(sid),
                                })
                                .expect("Failed to send event button click");
                            }
                        }
                    };
                }
            } else {
                ui.label("This plugin is mysterious...");
            }
        })
    }

    pub fn visible(&mut self, b: bool) -> &mut Self {
        self.visible = b;
        self
    }

    pub fn toggle_visible(&mut self) {
        self.visible = !self.visible;
    }

    pub fn resizable(&mut self, b: bool) -> &mut Self {
        self.resizable = b;
        self
    }

    pub fn closable(&mut self, b: bool) -> &mut Self {
        self.closable = b;
        self
    }

    pub fn title(&mut self, s: String) -> &mut Self {
        self.title = s;
        self
    }

    pub fn insert_widget(&mut self, id: String, widget: MiniWidget) {
        for (i, (wid, _)) in self.widgets.iter().enumerate() {
            if wid == &id {
                self.widgets[i] = (id, widget);
                return;
            }
        }
        self.widgets.push((id, widget));
    }
}
