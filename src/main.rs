mod custom_window;
mod plugin_mgr;

use plugin_mgr::PluginManager;

use bevy::prelude::*;
use bevy_egui::egui::Frame;
use bevy_egui::{egui, EguiContext, EguiPlugin};

use crate::custom_window::{CustomWindow, MiniWidget};
use crate::plugin_mgr::{FromEventType, FromPluginEvent, ToPluginEvent};
use crossbeam::channel::{Receiver, Sender, TryRecvError};
use std::collections::HashMap;

fn ui(
    mut egui_context: ResMut<EguiContext>,
    mut state: ResMut<State>,
    time: Res<Time>,
) {
    let ctx = &mut egui_context.ctx;

    egui::TopPanel::top("top_panel").show(ctx, |ui| {
        // The top panel is often a good place for a menu bar:
        egui::menu::bar(ui, |ui| {
            egui::menu::menu(ui, "File", |ui| {
                if ui.button("Quit").clicked {
                    std::process::exit(0);
                }
            });
            egui::menu::menu(ui, "Plugins", |ui| {
                if state.plugins.len() == 0 {
                    ui.button("None Loaded");
                } else {
                    for plugin in &mut state.plugins.values_mut() {
                        if ui.button(&plugin.name).clicked {
                            plugin.about.toggle_visible();
                        }
                    }
                }
            });
        });
    });

    egui::CentralPanel::default().show(ctx, |ui| {
        ui.label(format!("FPS: {:.0}", 1.0 / time.delta_seconds_f64()));
    });

    let frame = state.style;

    let event_tx = state.event_tx.clone();
    let plugins = &mut state.plugins;
    for plugin in plugins.values_mut() {
        plugin.about.show(ctx, frame, &event_tx);
    }

    match state.event_rx.try_recv() {
        Ok(event) => {
            let plugin = state.plugins.get_mut(&event.plugin).unwrap();
            match event.event_type {
                FromEventType::RegisterAboutWindow(title, resize, visible, closable) => {
                    plugin
                        .about
                        .title(title)
                        .resizable(resize)
                        .visible(visible)
                        .closable(closable);
                }
                FromEventType::RegisterLabel(sid, text) => {
                    plugin.about.insert_widget(sid, MiniWidget::Label(text));
                }
                FromEventType::RegisterButton(sid, text) => {
                    plugin.about.insert_widget(sid, MiniWidget::Button(text));
                }
            }
        }
        Err(TryRecvError::Empty) => {}
        Err(e) => {
            panic!("Error receiving on from channel: {:?}", e);
        }
    }
}

fn init(mut egui_context: ResMut<EguiContext>, mut state: ResMut<State>) {
    let ctx = &mut egui_context.ctx;
    let mut less_shadow = (*ctx.style()).clone();
    less_shadow.visuals.window_shadow.extrusion = 6.0;
    state.style = Frame::window(&less_shadow);
}

struct Plugin {
    name: String,
    about: CustomWindow,
}

struct State {
    style: Frame,
    plugins: HashMap<u16, Plugin>,
    event_rx: Receiver<FromPluginEvent>,
    event_tx: Sender<ToPluginEvent>,
}

fn main() {
    let (txf, rxf) = crossbeam::channel::bounded(256);
    let (txt, rxt) = crossbeam::channel::bounded(256);
    let mut plugin_mgr = PluginManager::new(txf, rxt);
    let plugins = plugin_mgr.plugins();

    std::thread::spawn(move || {
        plugin_mgr.run();
    });

    App::build()
        .add_resource(ClearColor(Color::rgb(0.0, 0.0, 0.0)))
        .add_resource(State {
            style: Default::default(),
            plugins: plugins
                .iter()
                .map(|(id, name)| {
                    (
                        *id,
                        Plugin {
                            name: name.to_owned(),
                            about: CustomWindow::new(*id, name.to_owned()),
                        },
                    )
                })
                .collect(),
            event_rx: rxf,
            event_tx: txt,
        })
        .add_plugins(DefaultPlugins)
        .add_plugin(EguiPlugin)
        .add_startup_system(init.system())
        .add_system(ui.system())
        .run();
}
