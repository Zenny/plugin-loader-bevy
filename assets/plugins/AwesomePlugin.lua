local cnt = 0

register_about_window("About: B)", false, false, false)
register_about_label("l1", "Hey man yeah this is cool bro they ain't kidding dawg")
register_about_label("l2", "yoyoyo")
register_about_label("l3", string.format("Inc: %d", cnt))
register_about_button("inc", "Increment!")

function button_click(id)
    if id == "inc" then
        cnt = cnt + 1
        register_about_label("l3", string.format("Inc: %d", cnt))
    end
end